(use-package yasnippet
  :ensure t
  :init
  (yas-global-mode 1)
  (setq yas-snippet-dirs (append yas-snippet-dirs
                                 '("~/skizo-emas/snippets")))
  )
