(use-package spacemacs-common
    :ensure spacemacs-theme
    :config
    (load-theme 'spacemacs-dark t)
 ;   (setq spacemacs-theme-org-agenda-height nil)
 ;   (setq spacemacs-theme-org-height nil)
    )
(use-package highlight-indent-guides
  :init
  (add-hook 'prog-mode-hook #'highlight-indent-guides-mode)
  (setq highlight-indent-guides-method 'column)
;;  (setq highlight-indent-guides-character ?\║)
  (setq highlight-indent-guides-responsive 'stack)
  (setq highlight-indent-guides-auto-even-face-perc 5)
  (setq highlight-indent-guides-auto-odd-face-perc 15)
  )
;; An atom-one-dark theme for smart-mode-line
(use-package smart-mode-line-atom-one-dark-theme
  :ensure t)

;; smart-mode-line
(use-package smart-mode-line
  :config
  (setq sml/theme 'atom-one-dark)
  (sml/setup))

(use-package sublimity
  :ensure t
  :config
  (require 'sublimity)
  (require 'sublimity-scroll)
  ;  (require 'sublimity-map)
  ;  (sublimity-map-set-delay 3))
  (setq sublimity-scroll-weight 12
        sublimity-scroll-drift-length 3)
  (sublimity-mode 1))

;;;;;;;;;;;; Highlighting ;;;;;;;;;;;;;;
;; highlight parentheses
(use-package highlight-parentheses
  :ensure t
  :diminish highlight-parentheses-mode
  :init (add-hook 'prog-mode-hook #'highlight-parentheses-mode)
  :config (set-face-attribute 'hl-paren-face nil :weight 'ultra-bold))

;; Interactively highlight the current-window (by dimming the others)
;; (use-package Dimmer
;;  :init (add-hook 'after-init-hook #'dimmer-mode))

;; Highlight symbol under cursor
(use-package symbol-overlay
  :ensure t
  :diminish symbol-overlay-mode
  :bind (("M-i" . symbol-overlay-put)
         ("M-n" . symbol-overlay-jump-next)
         ("M-p" . symbol-overlay-jump-prev)
         ([C-f3] . symbol-overlay-put)
         ([f3] . symbol-overlay-jump-next)
         ([S-f3] . symbol-overlay-jump-prev)
         ([M-f3] . symbol-overlay-remove-all))
  :init (add-hook 'prog-mode-hook #'symbol-overlay-mode))

;;;;;;;;;;;;; End Highlighting ;;;;;;;;;;;;;
