(use-package latex
    :mode
    ("\\.tex\\'" . latex-mode)
    :bind
    (:map LaTeX-mode-map
          ("M-<delete>" . TeX-remove-macro)
          ("C-c C-r" . reftex-query-replace-document)
          ("C-c C-g" . reftex-grep-document))
    :config
    (setq-default TeX-master nil ; by each new fie AUCTEX will ask for a master fie.
                  TeX-PDF-mode t
                  TeX-engine 'luatex)

    (setq TeX-auto-save t
          TeX-save-query nil       ; don't prompt for saving the .tex file
          TeX-parse-self t
          TeX-show-compilation nil         ; if `t`, automatically shows compilation log
          LaTeX-babel-hyphen nil ; Disable language-specific hyphen insertion.
          ;; `"` expands into csquotes macros (for this to work, babel pkg must be loaded after csquotes pkg).
          LaTeX-csquotes-close-quote "}"
          LaTeX-csquotes-open-quote "\\enquote{"
          TeX-file-extensions '("Rnw" "rnw" "Snw" "snw" "tex" "sty" "cls" "ltx" "texi" "texinfo" "dtx"))

    ;; Font-lock for AuCTeX
    ;; Note: '«' and '»' is by pressing 'C-x 8 <' and 'C-x 8 >', respectively
    (font-lock-add-keywords 'latex-mode (list (list "\\(«\\(.+?\\|\n\\)\\)\\(+?\\)\\(»\\)" '(1 'font-latex-string-face t) '(2 'font-latex-string-face t) '(3 'font-latex-string-face t))))
    ;; Add standard Sweave file extensions to the list of files recognized  by AuCTeX.
    (add-hook 'TeX-mode-hook (lambda () (reftex-isearch-minor-mode)))
    )




; http://nasseralkmim.github.io/notes/2016/08/21/my-latex-environment/
;; (use-package tex-site
;;   :ensure auctex
;;   :mode ("\\.tex\\'" . latex-mode)
;;   :config
;;   (setq TeX-auto-save t)
;;   (setq TeX-parse-self t)
;;   (setq-default TeX-master nil)
;;   (add-hook 'LaTeX-mode-hook
;;             (lambda ()
;;               (rainbow-delimiters-mode)
;;               (company-mode)
;;               (smartparens-mode)
;;               (turn-on-reftex)
;;               (setq reftex-plug-into-AUCTeX t)
;;               (reftex-isearch-minor-mode)
;;               (setq TeX-PDF-mode t)
;;               (setq TeX-source-correlate-method 'synctex)
;;               (setq TeX-source-correlate-start-server t)))

;; ;; Update PDF buffers after successful LaTeX runs
;; (add-hook 'TeX-after-TeX-LaTeX-command-finished-hook
;;            #'TeX-revert-document-buffer)

;; ;; to use pdfview with auctex
;; (add-hook 'LaTeX-mode-hook 'pdf-tools-install)

;; ;; to use pdfview with auctex
;; (setq TeX-view-program-selection '((output-pdf "pdf-tools"))
;;        TeX-source-correlate-start-server t)
;; (setq TeX-view-program-list '(("pdf-tools" "TeX-pdf-tools-sync-view"))))

(use-package reftex
  :ensure t
  :defer t
  :config
  (setq reftex-cite-prompt-optional-args t)); Prompt for empty optional arguments in cite

(use-package pdf-tools
  :ensure t
  :mode ("\\.pdf\\'" . pdf-tools-install)
  :bind ("C-c C-g" . pdf-sync-forward-search)
  :defer t
  :config
  (setq mouse-wheel-follow-mouse t)
  (setq pdf-view-resize-factor 1.10))
