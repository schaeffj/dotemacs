;; ;;;; Company auto completion
;; (use-package company
;;   :ensure t
;;   :diminish company-mode
;;   :defer t
;;   :config (global-company-mode))

(use-package auto-complete
  :ensure t
  :diminish auto-complete-mode
  :init
  (progn
    (ac-config-default)
    (global-auto-complete-mode t)
    (add-hook 'python-mode-hook 'jedi:setup)
    (setq jedi:setup-keys t)
    ; add org-mode to completion modes
    (add-to-list 'ac-modes 'org-mode)
    ))
;; Correction orthographique et syntaxique
(use-package flycheck
  :ensure t
  :diminish flycheck-mode
  :init (global-flycheck-mode)
  :config
  ;; Pour que l'insertion de dates dans orgmode fonctionne :
  (define-key flycheck-mode-map (kbd "C-c ! !") 'org-time-stamp-inactive)
  )

;; Smartparens
(use-package smartparens
  :ensure t
  :diminish smartparens-mode
  :config
  (progn
    (require 'smartparens-config)
    (smartparens-global-mode 1)))

(provide 'completion)
;;; completion.el ends here
