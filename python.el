(use-package highlight-indent-guides
  :ensure highlight-indent-guides
  :init
  (setq highlight-indent-guides-method 'character)
  (setq highlight-indent-guides-responsive 'stack)
  )

(use-package python-mode
  :init (progn
          (add-hook python-mode-hook 'highlight-indent-guides-mode)
          (add-hook python-mode-hook 'jedi-mode)
          )
  :config (progn
            (setq-default python-indent 4)
            (setq python-fill-docstring-style 'onetwo)
            (when (executable-find "ipython")
              (setq
               python-shell-interpreter "ipython"
               python-shell-interpreter-args ""
               python-shell-prompt-regexp "In \\[[0-9]+\\]: "
               python-shell-prompt-output-regexp "Out\\[[0-9]+\\]: "
               python-shell-completion-setup-code
               "from IPython.core.completerlib import module_completion"
               python-shell-completion-module-string-code
               "';'.join(module_completion('''%s'''))\n"
               python-shell-completion-string-code
               "';'.join(get_ipython().Completer.all_completions('''%s'''))\n")))
  )
